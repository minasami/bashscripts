#!/bin/sh
##start
##Update apt source list
sudo apt-get update
##Install Updates
sudo apt-get upgrade echo "Y"
#yes
#<<-EOF
#y
#EOF 

##confirm () {
    # call with a prompt string or use a default
##    read -r -p "${1:-Are you sure? [y/N]} " response
  ##  case $response in
 ##       [yY][eE][sS]|[yY]) 
   ##         ;;
    ##    *)
   ##         false
   ##         ;;
  ##  esac
##}
##Install Python Dependencies for OpenERP / Odoo
sudo apt-get install python-dateutil python-docutils python-feedparser python-jinja2 python-ldap python-libxslt1 python-lxml python-mako python-mock python-openid python-psycopg2 python-psutil python-pybabel python-pychart python-pydot python-pyparsing python-reportlab python-simplejson python-tz python-unittest2 python-vatnumber python-vobject python-webdav python-werkzeug python-xlwt python-yaml python-zsi poppler-utils python-pip yes
##Install supporting packages for OpenERP v8
sudo apt-get install gcc python-dev mc bzr python-setuptools python-babel python-feedparser python-reportlab-accel python-zsi python-openssl python-egenix-mxdatetime python-jinja2 python-unittest2 python-mock python-docutils lptools make python-psutil python-paramiko poppler-utils python-pdftools antiword postgresql yes

##Install Postgresql and GIT
sudo apt-get install python-software-properties 
##break
sudo add-apt-repository ppa:pitti/postgresql 
##break

sudo apt-get update 

sudo apt-get install postgresql-9.2 git-core yes

##Create Database user for OpenERP Odoo
sudo su postgres 
createuser openerp yes
##break
##Create Odoo user and group
sudo adduser --system --home=/opt/openerp --group openerp
##Download & Install Gdata
cd /opt/openerp
sudo wget http://gdata-python-client.googlecode.com/files/gdata-2.0.17.tar.gz

sudo tar zxvf gdata-2.0.17.tar.gz

sudo chown -R openerp: gdata-2.0.17

sudo -s

cd gdata-2.0.17/

python setup.py install

cd ..

exit
##Get lastest Odoo 8 from github repository
sudo git clone https://github.com/odoo/odoo.git

sudo chown -R openerp: odoo
##Create folder for custom and test addons
sudo mkdir custom-addons test-addons

sudo chown -R openerp: custom-addons

sudo chown -R openerp: test-addons
##Create Odoo Log file
sudo mkdir /var/log/openerp

sudo chown -R openerp:root /var/log/openerp
##Edit OpenERP (Odoo v8) configuration File
sudo cp /opt/openerp/odoo/debian/openerp-server.conf /etc/openerp-server.conf

sudo chown openerp: /etc/openerp-server.conf

sudo gedit /etc/openerp-server.conf

##Copy and paste below content in config file , write correct addons paths

[options]
; This is the password that allows database operations:

admin_passwd = PASSWORD

db_host = False

db_port = False

db_user = odoo

db_password = False

addons_path = /opt/odoo/v8/addons,/opt/odoo/v8/web/addons

;Log settings

logfile = /var/log/openerp/openerp-server.log

log_level = error
##SetUp OpenERP service file

sudo cp /opt/openerp/odoo/openerp-server /etc/init.d/openerp-server

sudo chmod 755 /etc/init.d/openerp-server

sudo chown root: /etc/init.d/openerp-server

sudo update-rc.d openerp-server defaults

sudo service openerp-server start
##Now Start OpenERP (Odoo) Server
cd /opt/openerp/odoo

./openerp-server
