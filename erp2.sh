#!/bin/sh
##start
##Update apt source list
sudo apt-get update
##Install Updates
sudo apt-get upgrade
pause ()
 {
 REPLY=Y
 while [ "$REPLY" == "Y" ] || [ "$REPLY" != "y" ]
 do
  echo -e "\t\tPress 'y' to continue\t\t\tPress 'n' to quit"
  read -n1 -s
      case "$REPLY" in
      "n")  exit                      ;;
      "N")  echo "case sensitive!!"   ;; 
      "y")  clear                     ;;
      "Y")  echo "case sensitive!!"   ;;
      * )  echo "Invalid Option"     ;;
 esac
 done
 }
##confirm () {
    # call with a prompt string or use a default
##    read -r -p "${1:-Are you sure? [y/N]} " response
  ##  case $response in
 ##       [yY][eE][sS]|[yY]) 
   ##         ;;
    ##    *)
   ##         false
   ##         ;;
  ##  esac
##}
##Install Python Dependencies for OpenERP / Odoo
sudo apt-get install python-dateutil python-docutils python-feedparser python-jinja2 python-ldap python-libxslt1 python-lxml python-mako python-mock python-openid python-psycopg2 python-psutil python-pybabel python-pychart python-pydot python-pyparsing python-reportlab python-simplejson python-tz python-unittest2 python-vatnumber python-vobject python-webdav python-werkzeug python-xlwt python-yaml python-zsi poppler-utils python-pip
##Install supporting packages for OpenERP v8
sudo apt-get install gcc python-dev mc bzr python-setuptools python-babel python-feedparser python-reportlab-accel python-zsi python-openssl python-egenix-mxdatetime python-jinja2 python-unittest2 python-mock python-docutils lptools make python-psutil python-paramiko poppler-utils python-pdftools antiword postgresql

##Install Postgresql and GIT
sudo apt-get install python-software-properties 

sudo add-apt-repository ppa:pitti/postgresql 

sudo apt-get update 

sudo apt-get install postgresql-9.2 git-core

##Create Database user for OpenERP Odoo
sudo su postgres
