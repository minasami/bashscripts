#!/bin/sh
## So first I have installed git my system, to work with Github
sudo apt-get install git yes
##Update apt source list
sudo apt-get update
##Download and Install Updates
sudo apt-get upgrade yes
##Install required packages for Odoo
sudo apt-get install graphviz ghostscript postgresql-client \
python-dateutil python-feedparser python-matplotlib \
python-ldap python-libxslt1 python-lxml python-mako \
python-openid python-psycopg2 python-pybabel python-pychart \
python-pydot python-pyparsing python-reportlab python-simplejson \
python-tz python-vatnumber python-vobject python-webdav \
python-werkzeug python-xlwt python-yaml python-imaging yes
##nstall supporting packages for Odoo
sudo apt-get install gcc python-dev mc bzr python-setuptools python-babel \
python-feedparser python-reportlab-accel python-zsi python-openssl \
python-egenix-mxdatetime python-jinja2 python-unittest2 python-mock \
python-docutils lptools make python-psutil python-paramiko poppler-utils \
python-pdftools antiword postgresql yes
##Install full Postgres database
sudo apt-get install postgresql
##Create database user for Odoo
sudo -u postgres createuser -s odoo
##Get / Install Odoo from Github to you system
wget -O- https://raw.githubusercontent.com/odoo/odoo/master/odoo.py | python
##Now go to odoo folder where opeperp-server file resides. (This command may vary to your system, if you have specify different path for storing odoo project)
cd ~/odoo/
##Run server
./openerp-server
sudo bash
# iptables -t nat -A PREROUTING -i eth0 -p tcp --dport 80 -j REDIRECT --to-port 8069
# iptables -t nat -A PREROUTING -i eth0 -p tcp --dport 443 -j REDIRECT --to-port 8070
# iptables-save
# exit
##
##
##
